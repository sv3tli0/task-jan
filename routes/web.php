<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', fn() => !Auth::guest() ? redirect()->route('dashboard') : view('welcome'))
    ->name('homepage');

Route::middleware('guest')->group(function () {
    Route::get('login', [AuthController::class, 'loginForm'])->name('login');
    Route::post('login', [AuthController::class, 'postLogin'])->name('login.post');
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', fn() => view('home'))
        ->name('dashboard');

    Route::group(['prefix' => 'books'], function () {
        Route::get('/create', [BookController::class, 'create'])->name('books.create');
        Route::get('/', [BookController::class, 'show'])->name('books.show');
        Route::post('/', [BookController::class, 'store'])->name('books.store');
    });

    Route::group(['prefix' => 'authors'], function () {
        Route::get('/', [AuthorController::class, 'search'])->name('authors.search');
        Route::get('/{authorId}', [AuthorController::class, 'show'])->name('authors.show');
        Route::delete('/{authorId}', [AuthorController::class, 'destroy'])->name('authors.delete');
    });

    Route::post('/logout', [AuthController::class, 'logout'])
        ->name('logout');

});
