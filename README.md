### Setup
1. Clone the project.
2. Install all PHP dependencies with `composer install`.
3. Install all JS the dependencies with `npm install` and execute `npm run build`.
4. Copy the `.env.example` file to `.env` and fill in the database credentials and the `REMOTE_API_BASE_URL` (if needed).
5. Run the sail container with `./vendor/bin/sail up -d` and run the migrations with `./vendor/bin/sail artisan migrate`.

### Usage
1. Go to `http://localhost` to see the application.
