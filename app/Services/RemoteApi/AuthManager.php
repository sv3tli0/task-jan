<?php

namespace App\Services\RemoteApi;

use App\Jobs\LogoutJob;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class AuthManager
{
    const SESSION_KEY = 'skeleton';

    public static function hasValidTokenData(): bool
    {
        return Session::has([
            self::SESSION_KEY.'.token',
            self::SESSION_KEY.'.expire',
            self::SESSION_KEY.'.refresh_token',
            self::SESSION_KEY.'.refresh_expire',
        ]) && Carbon::now()->lessThan(Carbon::parse(Session::get(self::SESSION_KEY.'.refresh_expire')));
    }

    public function user(): array
    {
        return Session::get(self::SESSION_KEY.'.user');
    }

    public function hasToken(): bool
    {
        return Session::has(self::SESSION_KEY.'.token');
    }
    public function hasRefreshToken(): bool
    {
        return Session::has(self::SESSION_KEY.'.refresh_token');
    }

    public function tokenExpired(): bool
    {
        return Carbon::now()->greaterThan(Carbon::parse(Session::get(self::SESSION_KEY.'.expire')));
    }

    public function refreshTokenExpired(): bool
    {
        return Carbon::now()->greaterThan(Carbon::parse(Session::get(self::SESSION_KEY.'.refresh_expire')));
    }

    /**
     * @throws \Exception
     */
    public function getToken(): string
    {
        if ($this->tokenExpired() && $this->hasRefreshToken()) {
            if ($this->refreshTokenExpired()) {
                $this->invalidate();
            }

            try {
                $this->refreshToken();
            } catch (\Exception $e) {
                $this->invalidate();
            }
        }

        if (! $this->hasToken()) {
            throw new \Exception('Token not found');
        }

        return Session::get(self::SESSION_KEY.'.token');
    }

    /**
     * @throws \Exception
     */
    public function authorize(string $email, string $password): ?array
    {
        $response = $this->client()->post('token', [
            'email' => $email,
            'password' => $password,
        ]);

        // In case of issues with API, throw exception.
        $response->throw();

        $this->storeTokenData($response->collect());

        return $this->user();
    }

    /**
     * Method to refresh token.
     * If something fails there will be thrown exception.
     *
     * @throws \Exception
     */
    public function refreshToken(): void
    {
        if (! is_string(Session::get(self::SESSION_KEY.'.refresh_token'))) {
            throw new \Exception('Refresh token not found');
        }

        $response = $this->client()->get('token/refresh/'.Session::get(self::SESSION_KEY.'.refresh_token'));
        if ($response->failed() || empty($response->json())) {
            throw new \Exception('Token refresh failed');
        }

        $this->storeTokenData($response->collect());
    }

    /**
     * @throws \Exception
     */
    private function storeTokenData(Collection $tokenData): void
    {
        if (! $tokenData->has(['user', 'token_key', 'refresh_token_key', 'expires_at', 'refresh_expires_at'])) {
            throw new \Exception('Token data is not valid');
        }

        Session::put(self::SESSION_KEY, [
            'user' => Arr::only($tokenData->get('user'), ['id', 'first_name', 'last_name', 'email']),
            'token' => $tokenData->get('token_key'),
            'refresh_token' => $tokenData->get('refresh_token_key'),
            'expire' => $tokenData->get('expires_at'),
            'refresh_expire' => $tokenData->get('refresh_expires_at'),
        ]);
    }

    /**
     * Separate client instance for authentication.
     *
     * @return PendingRequest
     */
    private function client(): PendingRequest
    {
        return Http::baseUrl(config('services.remote_api.base_url'))
            ->acceptJson()
            ->asJson();
    }

    /**
     * @return void
     * @throws AuthenticationException
     */
    public function invalidate(): void
    {
        Session::forget(self::SESSION_KEY);

        throw new AuthenticationException('Unauthorized', ['web'], '/');
    }
}
