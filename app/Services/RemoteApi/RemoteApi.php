<?php

namespace App\Services\RemoteApi;

use App\Services\RemoteApi\Resources\AuthorResource;
use App\Services\RemoteApi\Resources\BookResource;
use App\Services\RemoteApi\Resources\TagResource;
use App\Services\RemoteApi\Resources\UserResource;

/**
 * Simple Facade
 */
class RemoteApi
{
    public function auth(): AuthManager
    {
        return resolve(AuthManager::class);
    }

    public function authors(): AuthorResource
    {
        return resolve(AuthorResource::class);
    }

    public function books(): BookResource
    {
        return resolve(BookResource::class);
    }

    public function tags(): TagResource
    {
        return resolve(TagResource::class);
    }

    public function users(): UserResource
    {
        return resolve(UserResource::class);
    }
}
