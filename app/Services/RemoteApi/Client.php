<?php

namespace App\Services\RemoteApi;

use Exception;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Client
{
    private string $url;

    public function __construct(
        protected AuthManager $authManager,
    ) {
        $this->url = config('services.remote_api.base_url');
    }

    public function post(string $path, array $data = []): Collection
    {
        return $this->request('POST', $path, $data);
    }

    public function get(string $path, array $data = []): Collection
    {
        return $this->request('GET', $path, $data);
    }

    public function put(string $path, array $data = []): Collection
    {
        return $this->request('PUT', $path, $data);
    }

    public function patch(string $path, array $data = []): Collection
    {
        return $this->request('PATCH', $path, $data);
    }

    public function delete(string $path, array $data = []): bool
    {
        $this->request('DELETE', $path, $data);

        // If not failed we have succeeded.
        return true;
    }

    private function client(): PendingRequest
    {
        return Http::baseUrl($this->url)
            ->acceptJson()
            ->asJson()
            ->withToken($this->authManager->getToken());
    }

    /**
     * Request execution and error handling.
     *
     * @param string $method
     * @param string $path
     * @param array $data
     * @param bool $autoRefreshToken
     *
     * @return Collection
     */
    private function request(string $method, string $path, array $data, bool $autoRefreshToken = true): Collection
    {
        try {
            $response = match (Str::upper($method)) {
                'GET' => $this->client()->get($path, $data),
                'POST' => $this->client()->post($path, $data),
                'PUT' => $this->client()->put($path, $data),
                'PATCH' => $this->client()->patch($path, $data),
                'DELETE' => $this->client()->delete($path, $data),
                default => throw new \Exception('Invalid request method: ' . $method),
            };

            if ($response->failed()) {
                throw new \Exception('API request failed: ' . $response->status());
            }

            return $response->collect();
        } catch (UnauthorizedHttpException $e) {
            if ($autoRefreshToken) {
                // Try to Refresh token and retry again.
                $this->authManager->refreshToken();

                return $this->request($method, $path, $data, false);
            }
        } catch (Exception $e) {
            // Log helpful error information.
            logger()->error('Invalid API request exception', [
                'exception' => $e,
                'method' => $method,
                'path' => $path,
                'data' => $data,
            ]);
        }

        // Empty collection for default if nothing was returned until now.
        // Seems easier to handle than returning null.
        return collect();
    }
}
