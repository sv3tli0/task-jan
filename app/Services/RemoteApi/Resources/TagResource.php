<?php

namespace App\Services\RemoteApi\Resources;

use Illuminate\Support\Collection;

class TagResource extends AbstractResource
{
    protected string $resourcePath = 'tags';

    protected array $supportedOperations = [
        'search',
        'show',
        'update',
        'destroy',
    ];
}
