<?php

namespace App\Services\RemoteApi\Resources;

use Illuminate\Support\Collection;

interface ResourceInterface
{
    public function search(array $searchParams = []): Collection;

    public function show(string $entityId): Collection;

    public function update(string $entityId, array $data): Collection;

    public function destroy(string $entityId): bool;

}
