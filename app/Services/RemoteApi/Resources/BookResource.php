<?php

namespace App\Services\RemoteApi\Resources;

use Illuminate\Support\Collection;

class BookResource extends AbstractResource
{
    protected string $resourcePath = 'books';

    protected array $supportedOperations = [
        'search',
        'show',
        'update',
        'destroy',
        'store',
    ];

    public function storeValidationRules(): array
    {
        return [
            'title' => 'required|string',
            'release_date' => 'required|date',
            'description' => 'required|string',
            'isbn' => 'required|string',
            'format' => 'required|string',
            'number_of_pages' => 'string',
            "author.id" => 'required|integer',
        ];
    }
}
