<?php

namespace App\Services\RemoteApi\Resources;

use Illuminate\Support\Collection;

class UserResource extends AbstractResource
{
    protected string $resourcePath = 'users';

    protected array $supportedOperations = [
        'search',
        'show',
        'update',
        'destroy',
    ];

    public function me(): Collection
    {
        return $this->client->get('me');
    }

    public function addTag(string $authorId, string $tagId): Collection
    {
        return $this->client->post("{$this->resourcePath}/{$authorId}/tags/{$tagId}");
    }

    public function removeTag(string $authorId, string $tagId): bool
    {
        return $this->client->delete("{$this->resourcePath}/{$authorId}/tags/{$tagId}");
    }
}
