<?php

namespace App\Services\RemoteApi\Resources;

use App\Services\RemoteApi\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

abstract class AbstractResource implements ResourceInterface
{
    protected string $resourcePath = '';

    /**
     * @var string[]
     */
    protected array $supportedOperations = [
//        'search',
//        'show',
//        'update',
//        'destroy',
    ];
    protected array $searchFilters = [

    ];

    public function __construct(
        protected Client $client,
    ) { }

    public function search(array $searchParams = []): Collection
    {
        if (!in_array('search', $this->supportedOperations)) {
            throw new \BadMethodCallException("Method 'search' is not supported for this resource.");
        }

        $results = $this->client->get("{$this->resourcePath}", $searchParams)->collect();

        return collect([
            'meta' => Arr::except($results->toArray(), ['items']),
            'items' => collect($results->get('items')),
        ]);
    }

    public function show(string $entityId): Collection
    {
        if (!in_array('show', $this->supportedOperations)) {
            throw new \BadMethodCallException("Method 'show' is not supported for this resource.");
        }

        return collect($this->client->get("{$this->resourcePath}/{$entityId}"));
    }

    public function store(array $data): Collection
    {
        if (!in_array('store', $this->supportedOperations)) {
            throw new \BadMethodCallException("Method 'store' is not supported for this resource.");
        }

        return collect($this->client->post("{$this->resourcePath}"));
    }

    public function update(string $entityId, array $data): Collection
    {
        if (!in_array('update', $this->supportedOperations)) {
            throw new \BadMethodCallException("Method 'update' is not supported for this resource.");
        }

        return $this->client->put("{$this->resourcePath}/{$entityId}", $data);
    }

    public function destroy(string $entityId): bool
    {
        if (!in_array('destroy', $this->supportedOperations)) {
            throw new \BadMethodCallException("Method 'destroy' is not supported for this resource.");
        }

        return $this->client->delete("{$this->resourcePath}/{$entityId}");
    }

    public function getSearchFilters(): array
    {
        return $this->searchFilters;
    }

    protected function filterSearchParams(array $filters): array
    {
        return Arr::only($filters, $this->searchFilters);
    }
}
