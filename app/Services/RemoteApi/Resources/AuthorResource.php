<?php

namespace App\Services\RemoteApi\Resources;

use Illuminate\Support\Collection;

/**
 * Class AuthorResource.
 * Supports all standard CRUD operations.
 */
class AuthorResource extends AbstractResource
{
    protected string $resourcePath = 'authors';

    protected array $supportedOperations = [
        'search',
        'show',
        'update',
        'destroy',
    ];

    protected array $searchFilters = [
        'query' => ['sometimes', 'string', 'nullable',],
        'orderBy' => ['sometimes', 'string',],
        'direction' => ['sometimes', 'in:asc,desc',],
        'limit' => ['sometimes', 'integer',],
        'page' => ['sometimes', 'integer',],
    ];
}
