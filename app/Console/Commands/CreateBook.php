<?php

namespace App\Console\Commands;

use App\Services\RemoteApi\RemoteApi;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CreateBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'books:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating new book!';

    public function __construct(
        protected RemoteApi $remoteApi,
    ){
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! $this->mustAuthenticate()) {
            return Command::FAILURE;
        }

        $bookData = [];

        $bookData['author']['id'] = $this->getAuthorsChoice();

        foreach (['title', 'description', 'release_date', 'isbn', 'format', 'number_of_pages' ] as $field) {
            $bookData[$field] = $this->requestInputValue($field);
        }

        try {
            if ($this->remoteApi->books()->store($bookData)) {
                $this->info('Book has been created!');

                return Command::SUCCESS;
            } else {
                $this->error('Something went wrong!');

                return Command::FAILURE;
            }
        } catch (\Exception $e) {
            $this->error('Failed at the api on creating the book!', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);

            return Command::FAILURE;
        }
    }

    private function requestInputValue(string $field): string
    {
        $value = trim($this->ask(match ($field) {
            'title' => 'What is the title of the book?',
            'description' => 'What is the description of the book?',
            'release_date' => 'What is the release date of the book?',
            'isbn' => 'What is the ISBN of the book?',
            'format' => 'What is the format of the book?',
            'number_of_pages' => 'What is the number of pages of the book?',
            default => 'What is the value of the field?',
        }));

        $validate = Validator::make([$field => $value], [$field => match ($field) {
            'title' => ['required', 'string', 'min:3'],
            'description' => ['required', 'string', 'min:10', 'max:1000'],
            'release_date' => ['required', 'date', 'date_format:Y-m-d', 'before_or_equal:today'],
            'isbn' => ['required', 'string', 'min:10', 'max:13'],
            'format' => ['required', 'string', 'in:paperback,hardcover,electronic'],
            'number_of_pages' => ['required', 'integer', 'min:1', 'max:10000'],
            default => ['required', 'string'],
        }]);

        if ($validate->fails()) {
            $this->error($validate->errors()->first());

            return $this->requestInputValue($field);
        }

        return $value;
    }

    private function getAuthorsChoice(): int
    {
        $authors = Cache::remember(
            'authors',
            60, // no need of monitoring all the time changes in authors, so we can cache it for 1 minute.
            fn() => $this->remoteApi->authors()->search(['limit' => 1000])->get('items')
        )->toArray();

        $authors = Arr::map($authors, function($author) {
            return ['id' => $author['id'], 'name' => "{$author['first_name']} {$author['last_name']}"];
        });

        $name = $this->choice('Who is author of the book?', Arr::pluck($authors, 'name', 'id'));

        $index = array_search($name, array_column($authors, 'name'));

        if (empty($authors[$index]) || empty($authors[$index]['id'])) {
            $this->error('Invalid author!');

            return $this->getAuthorsChoice();
        }

        return $authors[$index]['id'];
    }

    private function mustAuthenticate(int $retry = 0): bool
    {
        if ($retry > 3) {
            $this->error('You have reached the maximum number of attempts!');

            return false;
        }

        $email = $this->ask('What is your email?');
        $password = $this->secret('What is your password?');

        if ($this->remoteApi->auth()->authorize($email, $password)) {
            return true;
        }

        $this->error('Invalid credentials!');

        return $this->mustAuthenticate($retry++);
    }
}
