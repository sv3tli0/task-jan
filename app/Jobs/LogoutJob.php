<?php

namespace App\Jobs;

use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutJob
{
    use Dispatchable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected Request $request)
    { }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Auth::guard('web')->logout();

        $this->request->session()->invalidate();

        $this->request->session()->regenerateToken();
    }
}
