<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Author extends Model
{
    protected $fillable = [
        'id',
        'books',
        'first_name',
        'last_name',
        'birthday',
        'gender',
        'place_of_birth',
    ];

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function setBooksAttribute(mixed $books): void
    {
        $this->attributes['books'] = $books instanceof Collection
            ? $books
            : collect(Arr::wrap($books))->map(fn($book) => new Book($book));
    }
}
