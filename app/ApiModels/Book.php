<?php

namespace App\ApiModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Book extends Model
{
    protected $fillable = [
        'id',
        'title',
        'author',
        'pages',
        'gender',
        'place_of_birth',
    ];

    public function setAuthorAttribute($data): void
    {
        $this->setAttribute('author', $data instanceof Author ? $data : new Author($data));
    }
}
