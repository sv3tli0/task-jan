<?php

namespace App\Http\Middleware;

use App\Jobs\LogoutJob;
use App\Services\RemoteApi\AuthManager;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }


    public function handle($request, \Closure $next, ...$guards)
    {
        if (Auth::user() && ! AuthManager::hasValidTokenData()) {
            LogoutJob::dispatchSync($request);
        }

        $this->authenticate($request, $guards);

        return $next($request);
    }}
