<?php

namespace App\Http\Controllers;

use App\ApiModels\Author;
use App\Http\Requests\BookStoreRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BookController extends Controller
{
    public function create()
    {
        $authors = Cache::remember(
            'authors',
            60, // no need of monitoring all the time changes in authors, so we can cache it for 1 minute.
            fn() => $this->remoteApi->authors()->search(['limit' => 1000])->get('items')
        );

        $authors = collect($authors)->map(function($author) {
            return [
                'id' => $author['id'],
                'fullname' => $author['first_name'] . ' ' . $author['last_name'],
            ];
        })->pluck('fullname', 'id');

        return view('books.create', compact('authors'));
    }

    public function store(BookStoreRequest $request): RedirectResponse
    {
        // Although we do have validation rules in the BookStoreRequest,
        // One extra check will be done upon we still need to validate the request
        $validatedData = $request->validate(
            $this->remoteApi->books()->storeValidationRules(), $request->all()
        );

        $authors = $this->remoteApi->books()->store($request->validated());

        return redirect()
            ->route('authors.show', ['authorId' => $validatedData['author']['id']]);
    }
}
