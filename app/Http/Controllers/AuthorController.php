<?php

namespace App\Http\Controllers;

use App\ApiModels\Author;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class AuthorController extends Controller
{
    public function search(Request $request)
    {
        $searchParams = $request->validate(
            $this->remoteApi->authors()->getSearchFilters(), $request->all()
        );

        $authors = $this->remoteApi->authors()
            ->search($searchParams)
            ->get('items')
            ->map(fn($author) => new Author($author));

        return view('authors.list', compact('authors'));
    }

    public function show(string $authorId)
    {
        // Show author details and his books.
        $author = $this->remoteApi->authors()->show($authorId)
            ->toArray();
        $author = new Author($author ?? []);

        return view('authors.show', compact('author'));
    }

    public function destroy(string $authorId): RedirectResponse
    {
        $author = $this->remoteApi->authors()->show($authorId)->toArray();

        if (! empty($author['books'])) {
            return redirect()->route('authors.search')
                ->with(['error' => 'Author has books, so he cannot be deleted.']);
        }

        return redirect()->route('authors.search')
            ->with('message', $this->remoteApi->authors()->destroy($authorId) ? 'Deleted.' : 'Failed to delete.');
    }
}
