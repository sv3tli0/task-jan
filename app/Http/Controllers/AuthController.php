<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginPostRequest;
use App\Jobs\LogoutJob;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function postLogin(LoginPostRequest $request): RedirectResponse
    {
        try {
            $userData = $this->remoteApi->auth()
                ->authorize($request->input('email'), $request->input('password'));

            $user = User::firstOrCreate(['email' => $userData['email']], [
                'name' => "{$userData['first_name']} {$userData['last_name']}",
                'password' => '',
            ]);

            Auth::login($user, true);

            $request->session()->regenerate();

            return redirect()->intended('dashboard');

        } catch (\Exception $e) {
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ])->onlyInput('email');
        }
    }

    public function loginForm()
    {
        return view('auth.login');
    }

    /**
     * Destroy an authenticated session.
     */
    public function logout(Request $request): RedirectResponse
    {
        LogoutJob::dispatchSync($request);

        return redirect()->route('homepage');
    }
}
