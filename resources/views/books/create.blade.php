@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Add Book') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('books.store') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="author" class="col-md-4 col-form-label text-md-end">{{ __('Author') }}</label>

                                <div class="col-md-6">
                                    <select id="author" type="text" class="form-control @error('author') is-invalid @enderror" name="author" value="{{ old('author') }}" required>
                                        <option> -- SELECT AUTHOR --</option>
                                        @foreach($authors as $key => $author)
                                            <option value="{{ $key }}"
                                                @selected($key == old('author', request()->get('authorId')))>{{ $author }} </option>
                                        @endforeach
                                    </select>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="title" class="col-md-4 col-form-label text-md-end">{{ __('Title') }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="isbn" class="col-md-4 col-form-label text-md-end">{{ __('ISBN') }}</label>

                                <div class="col-md-6">
                                    <input id="isbn" type="text" class="form-control @error('isbn') is-invalid @enderror" name="isbn" value="{{ old('isbn') }}" required>

                                    @error('isbn')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="format" class="col-md-4 col-form-label text-md-end">{{ __('Format') }}</label>

                                <div class="col-md-6">
                                    <input id="format" type="text" class="form-control @error('format') is-invalid @enderror" name="format" value="{{ old('format') }}" required>

                                    @error('format')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="number_of_pages" class="col-md-4 col-form-label text-md-end">{{ __('Number of pages') }}</label>

                                <div class="col-md-6">
                                    <input id="number_of_pages" type="number" class="form-control @error('number_of_pages') is-invalid @enderror" name="number_of_pages" value="{{ old('number_of_pages') }}" required>

                                    @error('number_of_pages')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="release_date" class="col-md-4 col-form-label text-md-end">{{ __('Release Date') }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="datetime-local" class="form-control @error('release_date') is-invalid @enderror" name="release_date" value="{{ old('release_date') }}" required>

                                    @error('release_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save Book') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
