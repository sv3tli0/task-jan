@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('View Authors') }}</div>

                    <div class="card-body">
                        @if(session()->has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session()->get('error') }}
                            </div>
                        @endif
                        @if(session()->has('message'))
                            <div class="alert alert-info" role="alert">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr class="text-center">
                                    <th>Names</th>
                                    <th>Gender</th>
                                    <th>Birthday</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach($authors as $author)
                                @if($author->books)
                                    @dd($author->books)
                                @endif
                                <tr>
                                    <td>{{ $author->fullName }}</td>
                                    <td>{{ $author->gender }}</td>
                                    <td>{{ \Carbon\Carbon::parse($author->birthday)->format('d-m-Y') }}</td>
                                    <td class="text-center">
                                        <a
                                            class="btn btn-outline-dark btn-sm text-sm-center"
                                            href="{{ route('authors.show', ['authorId' => $author->id]) }}"
                                        >View</a>
                                        <a
                                            class="btn btn-success btn-sm text-sm-center"
                                            href="{{ route('books.create', ['authorId' => $author->id]) }}"
                                        >Add Book</a>
                                        <form method="post" class="form d-inline" action="{{ route('authors.delete', ['authorId' => $author->id]) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger btn-sm text-sm-center">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
