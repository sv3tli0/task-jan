@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('View Authors') }}</div>

                    <div class="card-body">
                        <h5 class="fw-bold m-3">Author information</h5>
                        <div class="row mx-5">
                            <div class="col-3 text-end align-items-end fw-bolder">
                                Author:
                            </div>
                            <div class="col-9 fst-italic fw-bolder">
                                {{ $author->fullName }}
                            </div>
                            <div class="col-3 text-end align-items-end fw-bolder">
                                Birthday:
                            </div>
                            <div class="col-9 fst-italic fw-bolder">
                                {{ $author->birthDay }}
                            </div>
                        </div>

                        <hr class="mx-5 my-3">
                        <h5 class="fw-bold m-3">
                            Books
                            <a class="ml-5 btn btn-sm btn-success" href="{{ route('books.create', ['authorId' => $author->id]) }}">Add new book</a>
                        </h5>

                        <div class="row mx-5">
                            @if ($author->books->isEmpty())
                            <div class="alert alert-danger">
                                <p class="text-center">This author has no books.</p>
                            </div>
                            @else
                            <div class="col">
                                <table class="table table-responsive table-bordered">
                                    <thead>
                                        <tr class="text-center">
                                            <th>ID</th>
                                            <th>Title</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($author->books as $book)
                                            <tr>
                                                <td class="text-center">{{ $book->id }}</td>
                                                <td>{{ $book->title }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
